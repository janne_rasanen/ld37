﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VrActivator : MonoBehaviour {
    public GameObject vrCam;
	
	void Awake () {
		if (SteamVR.active)
        {
            gameObject.SetActive(false);
            vrCam.SetActive(true);
        }
	}
}
