﻿using UnityEngine;
using System.Collections;
using RogoDigital;

public class MovementRandomizer : MonoBehaviour {
    public string layerName;
    public string[] triggers;

    private int layerIndex;
    private int idleState;

    public float chancePerSec = 0.25f;
    private EyeController eyeController;
    private ScoreCounter scoreCounter;

    private Animator ac;
	void Start () {
        ac = GetComponentInChildren<Animator>();
        layerIndex = ac.GetLayerIndex(layerName);
        idleState = Animator.StringToHash("idle");
        eyeController = GetComponent<EyeController>();
        scoreCounter = FindObjectOfType<ScoreCounter>();

        Cloth c;
	}
	
	void Update () {
        bool isIdle = ac.GetCurrentAnimatorStateInfo(layerIndex).shortNameHash == idleState;

        eyeController.SetLookAtAmount(isIdle ? 1f : 0f);
        // scoreCounter.moodMultiplier = isIdle ? 1f : 0.1f;

        if (!isIdle)
            return;

        if (triggers.Length == 0)
            return;

        if (Random.Range(0f, 1f) > chancePerSec * Time.deltaTime)
            return;

        string randomTrigger = triggers[Random.Range(0, triggers.Length)];
        ac.SetTrigger(randomTrigger);
	}
}
