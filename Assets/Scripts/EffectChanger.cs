﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectChanger : MonoBehaviour {

    private OilPaint.OilPaint oilPaint;

    public float[] effects;

    private int currentEffect = 0;

    public float changeSpeed = 30f;

	void Start () {
        oilPaint = GetComponent<OilPaint.OilPaint>();
        oilPaint.amount = effects[currentEffect] / 100f;
    }
	
	void Update () {
		if (Input.GetMouseButtonDown(0))
        {
            currentEffect++;
            if (currentEffect >= effects.Length)
                currentEffect = 0;    
        }

        oilPaint.amount = Mathf.Clamp01(Mathf.MoveTowards(oilPaint.amount, effects[currentEffect] / 100f, Time.deltaTime * changeSpeed / 100f));
    }
}
