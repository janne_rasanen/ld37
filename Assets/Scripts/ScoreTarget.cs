﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ScoreTarget : MonoBehaviour {
    public float points = 1f;
    public Image indicatorImage;

    private ScoreCounter scoreCounter;
    private ParticleSystem heartEmitter;

    void Start () {
        scoreCounter = FindObjectOfType<ScoreCounter>();
        heartEmitter = GetComponentInChildren<ParticleSystem>();
    }
	
	public void OnScore()
    {
        scoreCounter.points += points;
        heartEmitter.Emit(3);
    }

    public void ReportProgress(float p)
    {
        indicatorImage.fillAmount = p;
    }
}
