﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneCycler : MonoBehaviour {

    private MoveToScene moveToScene;
	void Start () {
        moveToScene = GetComponent<MoveToScene>();
	}
	
	void Update () {
        if (Input.GetMouseButtonDown(1))
            moveToScene.LoadNextSceneCyclic();
	}
}
