﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoodTarget : MonoBehaviour {
    private ScoreCounter scoreCounter;

	void Start () {
        scoreCounter = FindObjectOfType<ScoreCounter>();
	}
	
	public void OnLookedLongEnough()
    {
        // change the mood, play a sound
        scoreCounter.MoodWorsens();
    }

    public void ReportProgress(float p)
    {
        scoreCounter.MoodProgress(p);
    }
}
