﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;

public class EyeTarget : MonoBehaviour {

    public float cycleSeconds = 2f;
    public float cooldownAfterScoring = 1f;
    public float decayRate = 0.5f; // units of a second in a second
    public UnityEvent triggerEvent;

    [System.Serializable]
    public class ProgressEvent : UnityEvent<float> { }

    public ProgressEvent progressEvent;

    private bool isBeingObserved = false;
    private float secsLookedAt = 0f;
    
    private float coolDownLeft = 0f;
    
    void Start () {
        
    }
	
	void LateUpdate () {
        if (coolDownLeft > 0)
        {
            coolDownLeft -= Time.deltaTime;
            return;
        }

        float progress;

        if (isBeingObserved)
        {
            progress = secsLookedAt / cycleSeconds;

            if (progress >= 1f)
            {
                secsLookedAt = 0;
                progress = 0f;
                coolDownLeft = cooldownAfterScoring;
                triggerEvent.Invoke();
            }            
        }
        else
        {
            secsLookedAt -= Time.deltaTime * decayRate;
            if (secsLookedAt < 0)
                secsLookedAt = 0;
            progress = secsLookedAt / cycleSeconds;
        }

        progress = Mathf.Clamp01(progress);
        progressEvent.Invoke(progress);

        isBeingObserved = false;
    }

    public void MarkSeen()
    {
        if (coolDownLeft > 0)
            return;

        isBeingObserved = true;
        secsLookedAt += Time.deltaTime;
    }
}
