﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MoveToScene : MonoBehaviour {

	
	public void ReloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex, LoadSceneMode.Single);
	}
	
	public void LoadNextSceneCyclic()
    {
        int nextSceneIndex = SceneManager.GetActiveScene().buildIndex + 1;
        if (nextSceneIndex >= SceneManager.sceneCountInBuildSettings)
            nextSceneIndex = 1;

        SceneManager.LoadScene(nextSceneIndex);
    }
}
