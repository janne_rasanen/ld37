﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class ClothApply : MonoBehaviour {

    [MenuItem("CONTEXT/Cloth/Zero all")]
    static void DoubleMass(MenuCommand command)
    {
        Cloth cloth = (Cloth)command.context;

        ClothSkinningCoefficient[] effs = cloth.coefficients;
        Debug.Log("Zeroing " + effs.Length + " coefficients..");
        for(int i=0; i < effs.Length; i++)
        {
            effs[i].collisionSphereDistance = 0;
            effs[i].maxDistance = 0;
        }
        cloth.coefficients = effs;
        Debug.Log("Done!");
    }
}
