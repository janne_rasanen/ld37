﻿using UnityEngine;
using System.Collections;

public class Positioner : MonoBehaviour {
    public string parentName;
    public Vector3 localOffset;

	void Start () {
        transform.localPosition = localOffset;

        if (string.IsNullOrEmpty(parentName))
            return;

        if (transform.parent.name.Equals(parentName))
            return;

        GameObject go = GameObject.Find(parentName);

        if (!go)
            return;

        transform.SetParent(go.transform, false);
    }

    
    // used when seeking the correct offset
    /*
    void Update()
    {
        transform.localPosition = localOffset;
    }
    */
    
}
