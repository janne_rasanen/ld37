﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEyes : MonoBehaviour {

    private Camera cam;
    private Vector3 middlePoint = new Vector3();
    private RaycastHit hitInfo;
    public LayerMask targetLayers;

	void Start () {
        cam = Camera.main;
	}
	
	void Update () {
        UpdateMiddlePoint();

        Ray r = cam.ScreenPointToRay(middlePoint);

        Debug.DrawRay(r.origin, r.direction * 3f, Color.white);

        bool b = Physics.Raycast(r, out hitInfo, Mathf.Infinity, targetLayers);

        if (!b)
            return;            
        
        EyeTarget[] tgts = hitInfo.collider.GetComponents<EyeTarget>();

        for (int i = 0; i < tgts.Length; i++)
            tgts[i].MarkSeen();
	}

    private void UpdateMiddlePoint()
    {
        middlePoint.Set(cam.pixelWidth / 2f, cam.pixelHeight / 2f, 1f);
    }
}
