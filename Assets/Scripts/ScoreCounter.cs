﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Linq;
using UnityStandardAssets.Characters.FirstPerson;
using System;

public class ScoreCounter : MonoBehaviour {
    private Camera cam;
    public Text score, time;
    public int roundLengthSecs = 60;

    private float startTime;
    public float points = 0f;

    public SkinnedMeshRenderer character;

    public GameObject gameoverWin, gameoverLose;

    private FirstPersonController fpc;

    const int totalMoodSteps = 3;
    private int moodStepsLeft = totalMoodSteps;
    private bool gameOver = false;

    public string blendshapeMoodProgress, blendshapeMoodStage, blendshapePositive;

    private int blendshapeIndexMoodProgress, blendshapeIndexMoodStage, blendshapeIndexPositive;

    private float moodStateProgress = 0f;
    private float moodHappy = 0f;
    private float moodStage = 0f;
    const float secsToReachHappy = 1.5f;

    void Start () {
        fpc = FindObjectOfType<FirstPersonController>();
        startTime = Time.time;
        cam = Camera.main;

        blendshapeIndexMoodProgress = character.sharedMesh.GetBlendShapeIndex(blendshapeMoodProgress);
        blendshapeIndexMoodStage = character.sharedMesh.GetBlendShapeIndex(blendshapeMoodStage);
        blendshapeIndexPositive = character.sharedMesh.GetBlendShapeIndex(blendshapePositive);
    }
	
	void Update () {
        int secsLeft = roundLengthSecs - Mathf.RoundToInt(Time.time - startTime);
        if (secsLeft < 0)
        {
            GameOver(gameoverWin);
            return;
        }
        time.text = secsLeft.ToString();
        score.text = GetPointString();

        if (moodStateProgress <= 0)
        {
            moodHappy += Time.deltaTime * (100f / secsToReachHappy);
        }
        else
        {
            moodHappy -= Time.deltaTime * (100f / secsToReachHappy * 2f);
        }

        character.SetBlendShapeWeight(blendshapeIndexPositive, moodHappy);
        character.SetBlendShapeWeight(blendshapeIndexMoodProgress, moodStateProgress);

        // lerp to mood stage weight
        moodStage = Mathf.MoveTowards(moodStage, 100f - (float)moodStepsLeft / totalMoodSteps * 100f, Time.deltaTime * 100f);
        character.SetBlendShapeWeight(blendshapeIndexMoodStage, moodStage);
    }

    private void GameOver(GameObject go)
    {
        if (gameOver)
            return;

        gameOver = true;
        go.SetActive(true);
        enabled = false;
        Destroy(fpc);
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    public string GetPointString()
    {
        return Mathf.RoundToInt(points).ToString();
    }

    public void MoodWorsens()
    {
        if (gameOver)
            return;

        moodStepsLeft--;
        if (moodStepsLeft <= 0)
        {
            GameOver(gameoverLose);
        }
    }

    public void MoodProgress(float p)
    {
        moodStateProgress = p * 100f;
    }
}
