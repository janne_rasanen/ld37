﻿using UnityEngine;
using System.Collections;
using System;

public class TitPhysics : MonoBehaviour {
    public string[] targetBoneNames;
	
	void Start () {
        JiggleBone bone = GetComponent<JiggleBone>();
        foreach (string boneName in targetBoneNames)
            CloneJiggle(bone, boneName);

        Destroy(bone);
	}

    private void CloneJiggle(JiggleBone bone, string boneName)
    {
        GameObject go = GameObject.Find(boneName);
        if (!go)
            return;

        go.AddComponent<JiggleBone>().GetCopyOf(bone);
    }
}
