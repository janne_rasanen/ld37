﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetVRPos : MonoBehaviour {

    public KeyCode reOrient;

    void Start () {
		
	}

    Transform reference
    {
        get
        {
            var top = SteamVR_Render.Top();
            return (top != null) ? top.origin : null;
        }
    }

    public Transform startPos;

    // Update is called once per frame
    void Update () {
        if (Input.GetKeyDown(reOrient) == true)
        {
            Debug.Log("Resetting");
            /*
            Quaternion q = transform.rotation;
            transform.rotation = Quaternion.identity;
            Valve.VR.OpenVR.System.ResetSeatedZeroPose();
            Valve.VR.OpenVR.Compositor.SetTrackingSpace(Valve.VR.ETrackingUniverseOrigin.TrackingUniverseSeated);
            transform.rotation = q;
            */
            reference.position = startPos.position;
            reference.rotation = startPos.rotation;
            
            // SteamVR.instance.hmd.ResetSeatedZeroPose();
            //UnityEngine.VR.InputTracking.Recenter();


            //transform.parent.localEulerAngles -= transform.localEulerAngles + transform.parent.localEulerAngles;
        }
    }
}
