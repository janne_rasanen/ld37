﻿using UnityEngine;
using System.Collections;

public class AnimatorTrigger : MonoBehaviour {
    private Animator anim;

	void Start () {
        anim = GetComponent<Animator>();
	}
	
	void Update () {
        if (Input.GetKeyDown(KeyCode.Alpha1))
            anim.SetTrigger("stretch");

        if (Input.GetKeyDown(KeyCode.Alpha2))
            anim.SetTrigger("batter");

        if (Input.GetKeyDown(KeyCode.Alpha3))
            anim.SetTrigger("disbelief");
    }
}
