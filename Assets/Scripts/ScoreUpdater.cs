﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreUpdater : MonoBehaviour {

	void Start () {
        GetComponent<Text>().text = "Score: " + FindObjectOfType<ScoreCounter>().GetPointString();
	}
}
